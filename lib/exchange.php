<?php

namespace Project\Cbr;

use SimpleXMLElement,
    Exception,
    Bitrix\Main\Config\Option,
    Project\Core\Utility;

class Exchange {

    const PATH = 'http://www.cbr.ru/scripts/XML_daily.asp';

    static public function getCours() {
        try {
            return Utility::useCache(array(__CLASS__, __FUNCTION__), function() {
                        $xml = file_get_contents(self::PATH);
                        if (empty($xml)) {
                            throw new Exception('Нет доступа');
                        }
                        $xml = new SimpleXMLElement($xml);
                        foreach ($xml->Valute as $arItem) {
                            if ((string) $arItem->CharCode === 'USD') {
                                $usd = (float) str_replace(',', '.', $arItem->Value);
                                if (empty($usd)) {
                                    throw new Exception('Ошибка при разборе файла');
                                }
                                Option::set("project.cbr", "usd", $usd);
                                return $usd;
                            }
                        }
                        throw new Exception('Не найдено');
                    }, Utility::CACHE_DAY);
        } catch (Exception $e) {
            pre($e->getMessage());
            return 0;
        }
    }

    static public function getUsd() {
        return Utility::useCache(array(__CLASS__, __FUNCTION__), function() {
                    $usd = Exchange::getCours();
                    if (empty($usd)) {
                        $usd = Option::get("project.cbr", "usd");
                        if (empty($usd)) {
                            $usd = Config::USD;
                        }
                    }
                    return $usd;
                }, Utility::CACHE_TIME);
    }

    static public function getPrice($price) {
//        pre(self::getUsd() , $price , Config::SURCHARGE);
        return self::getUsd() * $price * Config::SURCHARGE;
    }

}
