<?php

namespace Project\Cbr;

use Bitrix\Main\Application;

class Agent {

    static public function update() {
        Application::getConnection()->query("UPDATE b_iblock_element_prop_s2 SET PROPERTY_160 = IF(PROPERTY_159>0, PROPERTY_159*" . (Exchange::getUsd() * Config::SURCHARGE ) . ", PROPERTY_2)");
        return '\Project\Cbr\Agent::update();';
    }

}
