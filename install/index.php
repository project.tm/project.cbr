<?php

use Bitrix\Main\Application,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\EventManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Project\Core\Model\FavoritesTable,
    Project\Core\Model\RedirectTable;

IncludeModuleLangFile(__FILE__);

class PROJECT_CBR extends CModule {

    public $MODULE_ID = 'project.cbr';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');
        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJECT_CBR_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_CBR_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_CBR_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        Loader::includeModule($this->MODULE_ID);
        $this->InstallEvent();

        $agent = '\Project\Cbr\Agent::update();';
        $res = CAgent::GetList(Array(), Array("NAME" => $agent));
        if (!$res->Fetch()) {
            CAgent::AddAgent(
                    "\Project\Cbr\Agent::update();", // имя функции
                    $this->MODULE_ID, // идентификатор модуля
                    "N", // агент не критичен к кол-ву запусков
                    86400, // интервал запуска - 1 сутки
                    "", // дата первой проверки - текущее
                    "Y", // агент активен
                    "", // дата первого запуска - текущее
                    30);
        }
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        $this->UnInstallEvent();
        ModuleManager::unRegisterModule($this->MODULE_ID);
        CAgent::RemoveAgent('\Project\Cbr\Agent::update();', $this->MODULE_ID);
    }

    public function InstallEvent() {
        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler('main', 'OnBeforeUserUpdate', $this->MODULE_ID, '\Project\Sms\Event\User', 'OnBeforeUserUpdate');
    }

    public function UnInstallEvent() {
        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->unRegisterEventHandler('main', 'OnBeforeUserUpdate', $this->MODULE_ID, '\Project\Sms\Event\User', 'OnBeforeUserUpdate');
    }

}
